package com.zb.service;

import com.zb.entity.Houseinfo;

import java.util.List;

public interface HouseinfService {


    /**
     * 查询所有房源信息
     * @return
     */
    public List<Houseinfo> listHouseinfo();
}
