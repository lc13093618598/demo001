package com.zb.service.impl;

import com.zb.dao.HouseinfoDao;
import com.zb.entity.Houseinfo;
import com.zb.service.HouseinfService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
@Service
@Transactional
public class HouseinfServiceImpl  implements HouseinfService {
@Autowired
private HouseinfoDao houseinfoDao;


    @Override
    public List<Houseinfo> listHouseinfo() {
        return houseinfoDao.listHouseinfo();
    }
}
