package com.zb.dto;

public class ResponseEntity <T>{

    private int code; //状态码
    private T target; //目标内容    如果状态码是200   必须给予属性值
    private String message; //消息   如果状态码非100   必须给予属性值
    private String status; //状态

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public T getTarget() {
        return target;
    }

    public void setTarget(T target) {
        this.target = target;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
