package com.zb.entity;

import java.util.Date;

public class Houseinfo {
    private int houseid;
    private String housedesc;
    private Housetype  housetype;
    private double monthlyrent;
    private Date publishdate;

    public Housetype getHousetype() {
        return housetype;
    }

    public void setHousetype(Housetype housetype) {
        this.housetype = housetype;
    }

    public int getHouseid() {
        return houseid;
    }

    public void setHouseid(int houseid) {
        this.houseid = houseid;
    }

    public String getHousedesc() {
        return housedesc;
    }

    public void setHousedesc(String housedesc) {
        this.housedesc = housedesc;
    }



    public double getMonthlyrent() {
        return monthlyrent;
    }

    public void setMonthlyrent(double monthlyrent) {
        this.monthlyrent = monthlyrent;
    }

    public Date getPublishdate() {
        return publishdate;
    }

    public void setPublishdate(Date publishdate) {
        this.publishdate = publishdate;
    }
}
