package com.zb.controller;
import com.zb.dto.ResponseEntity;
import com.zb.entity.Houseinfo;
import com.zb.service.HouseinfService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
public class HouseController {
    @Autowired
    private HouseinfService houseinfService;
    //查询所有房屋列表
    @GetMapping("/listHouseinfo")
    @CrossOrigin
    public ResponseEntity<List<Houseinfo>>listHouseinfo(){
        ResponseEntity<List<Houseinfo>> entity=new ResponseEntity<>();
        try {
            List<Houseinfo> list= houseinfService.listHouseinfo();
            entity.setCode(HttpStatus.OK.value());
            entity.setTarget(list);
            System.out.println(list);
            System.out.println("zheng");
        } catch (Exception e) {
            System.out.println("c");
            e.printStackTrace();
            entity.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
            entity.setMessage("异常500");
        }
        return entity;
    }
}